CloudWatch Module
=================

Installs Amazon CloudWatch Agent. Relies on the availability of the package in some 
available repo, does not download it from Amazon.

Configuration file is generated from hiera directly converting hiera YAML to 
configuration file's JSON. The default is to return the information only about memory.

The default configuration in hiera:

```yaml
cloudwatch::settings:
  agent:
    metrics_collection_interval: 60
    run_as_user: root
  metrics:
    append_dimensions:
      AutoScalingGroupName: '${aws:AutoScalingGroupName}'
      ImageId: '${aws:ImageId}'
      InstanceId: '${aws:InstanceId}'
      InstanceType: '${aws:InstanceType}'
    metrics_collected:
      mem:
        measurement:
          - 'mem_used_percent'
        metrics_collection_interval: 60
```

is converted to JSON in `/opt/aws/amazon-cloud-agent/bin/config.json`:

```json
{
  "agent": {
    "metrics_collection_interval": 60,
    "run_as_user": "root"
  },
  "metrics": {
    "append_dimensions": {
      "AutoScalingGroupName": "${aws:AutoScalingGroupName}",
      "ImageId": "${aws:ImageId}",
      "InstanceId": "${aws:InstanceId}",
      "InstanceType": "${aws:InstanceType}"
    },
    "metrics_collected": {
      "mem": {
        "measurement": [
          "mem_used_percent"
        ],
        "metrics_collection_interval": 60
      }
    }
  }
}
```
